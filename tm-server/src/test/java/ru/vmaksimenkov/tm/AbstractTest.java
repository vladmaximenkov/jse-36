package ru.vmaksimenkov.tm;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.bootstrap.Bootstrap;
import ru.vmaksimenkov.tm.model.User;

public class AbstractTest {

    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final String TEST_USER_EMAIL = "test@user.email";
    @NotNull
    protected static final String TEST_USER_NAME = "test_user";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test_user_pass";
    @NotNull
    protected static User TEST_USER;
    @NotNull
    protected static String TEST_USER_ID;

}
