package ru.vmaksimenkov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.vmaksimenkov.tm.api.service.ServiceLocator;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class AdminUserEndpoint extends AbstractEndpoint implements ru.vmaksimenkov.tm.api.endpoint.IAdminUserEndpoint {

    public AdminUserEndpoint() {
        super(null);
    }

    public AdminUserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    public void lockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserByLogin(
            @WebParam(name = "session", partName = "session") @NotNull final Session session,
            @WebParam(name = "login", partName = "login") @NotNull final String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getUserService().unlockUserByLogin(login);
    }

}
