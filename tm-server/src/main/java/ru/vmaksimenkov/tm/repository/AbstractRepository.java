package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(@Nullable final E e) {
        list.add(e);
    }

    @Override
    public void addAll(@Nullable List<E> entityList) {
        if (entityList == null) return;
        list.addAll(entityList);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> operatedList = new ArrayList<>();
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(operatedList::add);
        list.removeAll(operatedList);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return list.stream().anyMatch(e -> userId.equals(e.getUserId()) && id.equals(e.getId()));
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull final E e) {
        list.stream().filter(item -> e.getId().equals(item.getId())).findFirst().ifPresent(list::remove);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public int size(@NotNull String userId) {
        return (int) list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .count();
    }

}
